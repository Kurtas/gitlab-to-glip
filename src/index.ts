import * as express from 'express'
import {Request, Response} from 'express'
import * as bodyParser from 'body-parser'
import {config} from './config'
import * as request from 'request'
import {GitlabParser} from './gitlab-parser'
import {GlipFormatter} from './glip-formatter'
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.post('/api/hook/:glipBlock', ({body, params: {glipBlock}}: Request, res: Response) => {
  if (glipBlock) {
    let news = GitlabParser.parse(body)
    console.log('extracted relevant information into a news -> ', JSON.stringify(news))
    let glipFormattedContent = GlipFormatter.format(news)
    new Promise((resolve, reject) => {
      request.post(
          `${config.glipWebHookURL}/${glipBlock}`,
          {
            headers: {contentType: 'application/json'},
            form: glipFormattedContent.toJson(),
            json: true
          },
          (err: boolean | null, res: any, body: any) => {
            if (err) {
              reject(err)
            } else {
              resolve(res)
            }
          }
      )
    }).then((result) => {
      res.status(204).json()
    }).catch((err) => {
      res.status(500).json(err)
    })
  } else {
    res.status(400).json({error: 'no valid glip block given'})
  }
})

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.redirect('/index.html')
})

let port = process.env['OPENSHIFT_NODEJS_PORT'] || 8080
let host = process.env['OPENSHIFT_NODEJS_IP'] || '127.0.0.1'

app.listen(+port, host, () => {
  console.log(`Listening on ${host}, server_port ${port}`)
})
