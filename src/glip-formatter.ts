import {GitlabNews, GitlabEventType} from './gitlab-parser'

export class GlipNotification {

  constructor(readonly icon: string,
              readonly activity: string,
              readonly title: string,
              readonly body: string) {
  }

  toJson(): Object {
    return {
      icon: this.icon,
      activity: this.activity,
      title: this.title,
      body: this.body
    }
  }
}

export class GlipFormatter {

  static formatGitlabEvent(eventType: GitlabEventType): string {
    let name = GitlabEventType[eventType].toLowerCase().replace('_', ' ') || ''
    return `${name.charAt(0).toUpperCase() || ''}${name.slice(1)}`
  }

  static fromNewsToTitle(news: GitlabNews): string {
    switch (news.eventType) {
      case GitlabEventType.PUSH:
        return `${news.userName} has just pushed ${news.gitlabNews['total_commits_count']} to `
            + `${news.project}#${news.gitlabNews.ref.split('/').slice(-1)[0]}`
      case GitlabEventType.ISSUE:
        let issueObj = news.gitlabNews['object_attributes']
        let {state: action} = issueObj
        return `${news.userName} has just ${action} ${(action === 'opened') ? 'an' : 'the'} issue `
            + `#${issueObj.id}: ${issueObj.title}`
      default:
        return `${GlipFormatter.formatGitlabEvent(news.eventType)} by ${news.userName} on ${news.project}`
    }
  }

  static fromNewsToActivity(news: GitlabNews): string {
    return `${GlipFormatter.formatGitlabEvent(news.eventType)} event on ${news.project}`
  }

  static fromNewsToContent(news: GitlabNews): string {
    switch (news.eventType) {
      case GitlabEventType.PUSH:
        return `${news.gitlabNews.commits.map(({url, author, message}: any, idx: number) => {
          message = message.replace('\n', ' ')
          message = (message.length < 50) ? message : `${message.substring(0, 47)}...`
          return `- **${author.name}**: [${message}](${url}) `
        }).join('\n')}
        [see more on Gitlab](${news.gitlabNews.project[`web_url`]}/tree/${news.gitlabNews.ref.split('/').slice(-1)[0]})`
      case GitlabEventType.ISSUE:
        let issueObj = news.gitlabNews['object_attributes']
        let description = issueObj.description.replace('\n', '\n\t')
          description = (description.length < 100)? description: `${description.substring(0, 97)}...`
        let bodyMessage = `_description_: \r\n${description}\r\n`
        let assignee = news.gitlabNews.assignee
        if (assignee) {
          bodyMessage += `_assignee_: ${assignee.name} (@${assignee.username})\r\n`
        }
        bodyMessage += `see the project [here](${news.gitlabNews.project['web_url']})`
        return bodyMessage
      default:
        return `see the project [here](${news.gitlabNews.project['web_url']})`
    }
  }

  static format(news: GitlabNews): GlipNotification {
    return new GlipNotification(
        (news.userAvatar) ?
            news.userAvatar :
            'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo-extra-whitespace.png',
        GlipFormatter.fromNewsToActivity(news),
        GlipFormatter.fromNewsToTitle(news),
        GlipFormatter.fromNewsToContent(news)
    )
  }
}
